#!/bin/bash
kill $(ps -aux | grep startup.sh | grep -v grep | awk '{print $2}')

export DISPLAY=:1
Xvfb :1 -screen 0 1600x900x16 &
sleep 5
openbox-session  &
x11vnc -display :1 -nopw -listen localhost -xkb -ncache 10 -ncache_cr -forever &
#cd /root/noVNC && ln -sf vnc_auto.html index.html && exec ./utils/launch.sh --vnc localhost:5900
cd /root/noVNC  && exec ./utils/launch.sh --vnc localhost:5900 
#adb install /opt/android-sdk-linux/platform-tools/whatspp.apk &
