FROM ubuntu:trusty

MAINTAINER hardikj "hardikjuneja.hj@gmail.com"

# ====================== update ubuntu ======================= #

RUN apt-get update -qq

# Dependencies to execute android
RUN cd /etc/apt/sources.list.d
RUN echo "deb http://old-releases.ubuntu.com/ubuntu/ raring main restricted universe multiverse" >ia32-libs-raring.list
#RUN apt-get install program:i386

RUN apt-get update
RUN apt-get install -y --no-install-recommends openjdk-7-jdk libgd2-xpm-dev lib32z1 lib32ncurses5 lib32bz2-1.0 lib32stdc++6 


# ====================== install main sdk ======================= #

# Main Android SDK
#RUN apt-get install -y --no-install-recommends wget
ADD android-sdk_r24.4.1-linux.tgz /opt

# Other tools and resources of Android SDK
ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH ${PATH}:${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools
#RUN echo y | android update sdk --filter platform-tools,build-tools-19.0.3,android-23,extra-android-support --no-ui --force

# Git to pull external repositories of Android app projects
RUN apt-get install -y --no-install-recommends git

# Cleaning
RUN apt-get clean

RUN echo y | android update sdk --filter platform-tools,build-tools-23.0.0,android-23,extra-android-support --no-ui --force
#RUN echo y | android update sdk -u -a -t 23
RUN echo y | android update sdk -u -a -t sys-img-x86-android-23 #system-image 
RUN echo y | android update sdk -u -a -t tool

# Set up and run emulator
# RUN echo no | android create avd --force -n test -t android-23
# Avoid emulator assumes HOME as '/'.
ENV HOME /root
ADD wait-for-emulator /usr/local/bin/
ADD start-emulator /usr/local/bin/

#RUN mkdir -p /opt/tmp && android create project -g -v 0.9.+ -a MainActivity -k com.example.example -t android-17 -p /opt/tmp
#RUN cd /opt/tmp && ./gradlew tasks
#RUN rm -rf /opt/tmp
#===================== install NO VNC =====================

RUN apt-get update -y && \
    apt-get install -y x11vnc python python-numpy Xvfb openbox  menu && \
    cd /root && git clone https://github.com/kanaka/noVNC.git && \
    cd noVNC/utils && git clone https://github.com/kanaka/websockify websockify && \
    cd /root 

ADD noVNC/index.html /root/noVNC/
ADD noVNC/include/base.css /root/noVNC/include/

ADD startup.sh /
ADD autostart /etc/xdg/openbox/autostart
ADD avd /root/.android/avd
RUN chmod 0755 /startup.sh && \
    apt-get autoclean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/*

ADD openbox-session /usr/bin/openbox-session
ADD environment /etc/xdg/openbox/environment
ADD whatspp.apk /opt/android-sdk-linux/platform-tools
#CMD bash -C '/startup.sh';'bash'
ENTRYPOINT ["/startup.sh"]
